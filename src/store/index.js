import Vue from "vue";
import Vuex from "vuex";
import createLogger from "vuex/dist/logger";
import { uuid } from "vue-uuid";
import { getField, updateField } from "vuex-map-fields";
import service from "../../service";
import { TABS } from "@/constants/tabs";
import { API_SUBMIT_FORM } from "../../endpoints";
import { rolesMergeHandler } from "../helpers/roles";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== "production";

const getDefaultState = () => {
  return {
    visibleTab: TABS.DETAILS,
    isPreview: false,
    form: {
      userDetails: {
        firstName: "",
        lastName: "",
        email: "",
        phone: ""
      },
      boardExperience: "",
      tbraProfile: "",
      tbraProfileList: [
        {
          id: uuid.v1(),
          company: "",
          entityType: "",
          roleType: "",
          tenure: ""
        }
      ],
      currentHistoricRolesList: [
        {
          id: uuid.v1(),
          from: "",
          to: "",
          position: "",
          organization: ""
        }
      ],
      currentHistoricRolesHeadlines: "",
      requiredForThisRoleSections: [],
      requiredForThisRoleRoles: [],
      desiredForThisRoleSections: [],
      desiredForThisRoleRoles: [],
      coverLetter: "",
      cv: {
        cvFile: "",
        cvCondition: ""
      }
    },
    data: {
      data: null,
      isError: false,
      isLoading: false
    },
    submit: {
      data: false,
      isError: false,
      isLoading: false
    }
  };
};

const store = new Vuex.Store({
  plugins: debug ? [createLogger()] : [],
  strict: debug,
  state: getDefaultState(),
  mutations: {
    GET_DATA_START(state) {
      state.data.data = null;
      state.data.isError = false;
      state.data.isLoading = true;
    },
    GET_DATA_SUCCESS(state, data) {
      state.data.isError = false;
      state.data.isLoading = false;
      state.data.data = data;
    },
    GET_DATA_ERROR(state) {
      state.data.data = null;
      state.data.isLoading = false;
      state.data.isError = true;
    },
    SUBMIT_DATA_START(state) {
      state.submit.data = false;
      state.submit.isError = false;
      state.submit.isLoading = true;
    },
    SUBMIT_DATA_SUCCESS(state) {
      state.submit.isError = false;
      state.submit.isLoading = false;
      state.submit.data = true;
    },
    SUBMIT_DATA_ERROR(state) {
      state.submit.data = false;
      state.submit.isLoading = false;
      state.submit.isError = true;
    },
    updateField,
    changeVisibleTab(state, tab) {
      state.isPreview = false;
      state.visibleTab = tab;
    },
    addRole(state, { field, data }) {
      state.form[field].push(data);
    },
    removeRole(state, { field, id }) {
      state.form[field] = state.form[field].filter(item => item.id !== id);
    },
    populateRequiredRoles(state, data) {
      const requiredSectionsData = [];
      const requiredRolesData = [];

      data &&
        data.required_for_this_role &&
        data.required_for_this_role.sections &&
        data.required_for_this_role.sections.length &&
        data.required_for_this_role.sections.map((item, index) => {
          // const id = uuid.v4();
          requiredSectionsData.push({
            id: index,
            title: item.section_text,
            condition: ""
          });
          requiredRolesData.push({
            section_id: index,
            id: uuid.v1(),
            role: "",
            details: ""
          });
        });

      state.form.requiredForThisRoleSections = requiredSectionsData;
      state.form.requiredForThisRoleRoles = requiredRolesData;
    },
    populateDesiredRoles(state, data) {
      const desiredSectionsData = [];
      const desiredRolesData = [];

      data &&
        data.desired_for_this_role &&
        data.desired_for_this_role.sections &&
        data.desired_for_this_role.sections.length &&
        data.desired_for_this_role.sections.map((item, index) => {
          // const id = uuid.v4();
          desiredSectionsData.push({
            id: index,
            title: item.section_button_text,
            condition: ""
          });
          desiredRolesData.push({
            section_id: index,
            id: uuid.v1(),
            role: "",
            details: ""
          });
        });

      state.form.desiredForThisRoleSections = desiredSectionsData;
      state.form.desiredForThisRoleRoles = desiredRolesData;
    },
    addCvFile(state, file) {
      state.form.cv.cvFile = file;
    },
    removeCvFile(state) {
      state.form.cv.cvFile = "";
    },
    resetState(state) {
      Object.assign(state, getDefaultState());
    },
    setPreview(state, value) {
      state.isPreview = value;
    }
  },
  getters: {
    getField
  },
  actions: {
    async fetchData({ commit }) {
      commit("GET_DATA_START");

      try {
        const slug = window.location.href.split("/")[3];
        if (!slug) {
          throw Error();
        }

        const url = `${process.env.VUE_APP_API_PATHNAME}${slug}`;
        const { data } = await service.get(url);

        if (!data || !data.length || !data[0].ACF) {
          throw Error();
        }

        commit("GET_DATA_SUCCESS", data[0].ACF);
        commit("populateRequiredRoles", data[0].ACF);
        commit("populateDesiredRoles", data[0].ACF);
      } catch (e) {
        commit("GET_DATA_ERROR");
        console.log(e);
      }
    },
    async submitData({ commit }) {
      commit("SUBMIT_DATA_START");

      try {
        const {
          userDetails,
          boardExperience,
          tbraProfile,
          tbraProfileList,
          currentHistoricRolesHeadlines,
          currentHistoricRolesList,
          requiredForThisRoleSections,
          requiredForThisRoleRoles,
          desiredForThisRoleSections,
          desiredForThisRoleRoles,
          coverLetter,
          cv
        } = this.state.form;

        const formData = new FormData();
        if (cv.cvCondition === "no" && cv.cvFile) {
          formData.append("file", cv.cvFile, cv.cvFile.name);
        }

        const finalData = {
          userDetails,
          boardExperience: {
            boardExperience,
            tbraProfile: boardExperience === "no" ? "no" : tbraProfile,
            tbraProfileList:
              boardExperience === "no" || tbraProfile === "yes"
                ? []
                : tbraProfileList
          },
          currentHistoricRoles: {
            list: currentHistoricRolesList,
            headline: currentHistoricRolesHeadlines
          },
          coverLetter,
          cv: {
            cv: cv.cvCondition
          },
          required: rolesMergeHandler(
            requiredForThisRoleSections,
            requiredForThisRoleRoles
          ),
          desired: rolesMergeHandler(
            desiredForThisRoleSections,
            desiredForThisRoleRoles
          )
        };

        const emailSubject = `App form - ${this.state.data.data.form_title}`;

        formData.append("data", JSON.stringify(finalData));
        formData.append("email_subject", JSON.stringify(emailSubject));

        await service.post(API_SUBMIT_FORM, formData, {
          headers: {
            "Content-Type": "multipart/form-data"
          }
        });

        commit("SUBMIT_DATA_SUCCESS");
      } catch (e) {
        commit("SUBMIT_DATA_ERROR");
        console.log(e, "submit error");
      }
    }
  }
});

export default store;
