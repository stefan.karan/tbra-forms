export const errorMixins = {
  methods: {
    formErrorMessagesHandler(field) {
      if (field.$error) {
        // Min Lenght
        if (field.minLength) {
          return `Required at least ${field.$params.minLength.min} row`;
        }

        // Required
        if (!field.required) {
          return "Required";
        }

        // minLength
        if (!field.email) {
          return "Enter a valid email";
        }
      }
      return "";
    }
  }
};
