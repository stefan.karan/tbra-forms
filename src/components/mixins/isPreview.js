import { mapState } from "vuex";
export const isPreviewMixin = {
  computed: {
    ...mapState(["isPreview"])
  }
};
