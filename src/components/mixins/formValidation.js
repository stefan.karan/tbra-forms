import { required, email, minLength } from "vuelidate/lib/validators";
export const formValidation = {
  validations() {
    let validationsData = {
      firstName: {
        required
      },
      lastName: {
        required
      },
      email: {
        required,
        email
      },
      phone: {
        required
      },
      coverLetter: {
        required
      },
      cvCondition: {
        required
      },
      boardExperience: {
        required
      },
      currentHistoricRolesList: {
        required,
        minLength: minLength(1),
        $each: {
          id: {},
          from: {
            required
          },
          to: {
            required
          },
          position: {
            required
          },
          organization: {
            required
          },
          $trackBy: "id"
        }
      },
      currentHistoricRolesHeadlines: {
        required
      },
      requiredForThisRoleSections: {
        $each: {
          id: {},
          condition: {
            required
          },
          $trackBy: "id"
        }
      },
      requiredForThisRoleRoles: {
        $each: {
          section_id: {},
          id: {},
          role: {},
          details: {},
          $trackBy: "id"
        }
      },
      desiredForThisRoleSections: {
        $each: {
          id: {},
          condition: {
            required
          },
          $trackBy: "id"
        }
      },
      desiredForThisRoleRoles: {
        $each: {
          section_id: {},
          id: {},
          role: {},
          details: {},
          $trackBy: "id"
        }
      }
    };

    if (this.cvCondition === "no") {
      validationsData = {
        ...validationsData,
        cvFile: {
          required
        }
      };
    }

    if (this.boardExperience === "yes") {
      validationsData = {
        ...validationsData,
        tbraProfile: {
          required
        }
      };
    }
    if (this.tbraProfile === "no") {
      validationsData = {
        ...validationsData,
        tbraProfileList: {
          required,
          minLength: minLength(1),
          $each: {
            id: {},
            company: {
              required
            },
            entityType: {
              required
            },
            roleType: {
              required
            },
            tenure: {
              required
            },
            $trackBy: "id"
          }
        }
      };
    }

    return validationsData;
  }
};
