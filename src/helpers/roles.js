export const rolesMergeHandler = (section, roles) => {
  if (!section || !section.length) return null;

  return section.map(section => {
    const _roles = roles.filter(role => role.section_id === section.id);

    return {
      section,
      roles: section.condition === "yes" ? _roles : []
    };
  });
};
