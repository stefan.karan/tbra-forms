import Vue from "vue";
import App from "./App.vue";
import store from "./store";
import UUID from "vue-uuid";
import Vuelidate from "vuelidate";
import vSelect from "vue-select";
import "vue-select/dist/vue-select.css";

Vue.component("vSelect", vSelect);

Vue.config.productionTip = false;
Vue.use(UUID);
Vue.use(Vuelidate);

new Vue({
  store,
  render: h => h(App)
}).$mount("#app");
