const path = require("path");

module.exports = {
  // publicPath: "",
  configureWebpack: {
    resolve: {
      alias: {
        "@": path.resolve(__dirname, "src/")
      }
    }
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: `
          @import "@/assets/scss/mixins/_all-mixins.scss";
          @import "@/assets/scss/variables/_all-variables.scss";
        `
      }
    }
  }
};
