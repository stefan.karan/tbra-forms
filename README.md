# tbra-forms-vue

## Project setup
Copy `.env.example` to `.env` for having environment variables necessary in project:
```bash
cp .env.example .env
```

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
